package kz.zhanbolat.di.container;

public interface DependencyInjectionContainer {
    Object getBean(String beanName);
}
